unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, REST.Types,
  System.Rtti, FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Grid, REST.Response.Adapter,
  REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, Data.Bind.EngExt,
  FMX.Bind.DBEngExt, Data.Bind.DBScope, FMX.Layouts, FMX.ListBox, KeyApi,
  FMX.DateTimeCtrls, FMX.Edit, FMX.StdCtrls;

type
  TDevise = class
  private
    FCode: string;
    FRate: Extended;
    procedure SetCode(const Value: string);
    procedure SetRate(const Value: Extended);
  public
    property Code: string read FCode write SetCode;
    property Rate: Extended read FRate write SetRate;
  end;

  TFrmMain = class(TForm)
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    ListBox1: TListBox;
    Label1: TLabel;
    Edit1: TEdit;
    btnOk: TButton;
    btnCancel: TButton;
    Panel1: TPanel;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.fmx}

uses
  DateUtils, System.JSON, System.JSON.Readers, System.JSON.Types,
  System.IOUtils;

procedure TFrmMain.btnOkClick(Sender: TObject);
begin
  ShowMessage('OK');
end;

procedure TFrmMain.btnCancelClick(Sender: TObject);
begin
  ShowMessage('Cancel');
end;

procedure TFrmMain.FormCreate(Sender: TObject);
var
  jsonFile, str: string;
  tms: Integer;
  fmt: TFormatSettings;
  d: TDateTime;
  b: Boolean;

  jsonOb: TJSOnObject;
  rates: TJSONValue;
  reader: TStringReader;
  txtReader: TJsonTextReader;
begin
  jsonFile := FormatDateTime('yyyymmdd".json"', Now);

  if TFile.Exists(jsonFile) then
    jsonOb := TJSOnObject.ParseJSONValue(TFile.ReadAllText(jsonFile))
      as TJSOnObject
  else
  begin
    RESTRequest1.Params[0].Value := TKeyFixer.GetKey;
    RESTRequest1.Execute;

    jsonOb := TJSOnObject.ParseJSONValue(RESTResponse1.JSONText) as TJSOnObject;

    TFile.WriteAllText(jsonFile, jsonOb.ToString);
  end;

  ListBox1.Items.Add(jsonOb.Values['success'].Value); // "true"
  if jsonOb.TryGetValue<Boolean>('success', b) then
    if (b) then
      ListBox1.Items.Add('True')
    else
      ListBox1.Items.Add('False')
  else
    ListBox1.Items.Add('-');

  if (b) then
  begin

    ListBox1.Items.Add(jsonOb.Values['timestamp'].Value);
    if jsonOb.TryGetValue<Integer>('timestamp', tms) then
      ListBox1.Items.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss.zzz',
        UnixToDateTime(tms)))
    else
      ListBox1.Items.Add('-');

    str := jsonOb.Values['base'].Value;
    ListBox1.Items.Add(str); // "eur"

    if jsonOb.TryGetValue<TDateTime>('date', d) then
      ListBox1.Items.Add('Date: ' + DateTimeToStr(d))
    else
      ListBox1.Items.Add('-');

    str := jsonOb.Values['date'].Value;
    ListBox1.Items.Add(str); // date

    fmt := TFormatSettings.Create;
    fmt.DateSeparator := '-';
    fmt.ShortDateFormat := 'yyyy-mm-dd';

    if TryStrToDateTime(str, d, fmt) then
      ListBox1.Items.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss.zzz', d))
    else
      ListBox1.Items.Add('-');

    ListBox1.Items.Add('------------------------------');

    try
      rates := jsonOb.GetValue<TJSOnObject>('rates');
      reader := TStringReader.Create(rates.ToJSON);
      try
        txtReader := TJsonTextReader.Create(reader);
        try
          while txtReader.Read do
            case txtReader.TokenType of
              TJsonToken.PropertyName:
                ListBox1.Items.Add('prop: ' + txtReader.Value.AsString);
              TJsonToken.Float:
                ListBox1.Items.Add
                  ('fl: ' + txtReader.Value.AsExtended.ToString);
            end;
        finally
          txtReader.Free;
        end;
      finally
        reader.Free;
      end;
    finally
      jsonOb.Free;
    end;
  end;
end;

{ TDevise }

procedure TDevise.SetCode(const Value: string);
begin
  FCode := Value;
end;

procedure TDevise.SetRate(const Value: Extended);
begin
  FRate := Value;
end;

end.
