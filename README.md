# fixer

Test API fixer with Delphi 10.4.2.
FMX application.

## Compilation

To compile the sources, you must fill in the API key.
Add unit `KeyApi.pas`  like this :

```pas
unit KeyApi;

interface

type
  TKeyFixer = Class
    public
      class function GetKey: string;
  End;

implementation

{ TKeyFixer }

class function TKeyFixer.GetKey: string;
begin
  // replace this value by your key
  Result := '42';
end;

end.
```

## License

MIT License
